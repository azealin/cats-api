let request = require("supertest");

request = request("https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com");
//hard setting the URL, maybe can pull this from somewhere later?
jest.setTimeout(10000);
describe("Search Endpoint Tests", () => {
  it("should search a cat", async () => {
    const response = await request.post("/cats").send({
      fullname: "Thor",
      color: "black",
      age: 12,
      ownersName: "Rexxar",
    });

    expect(response.statusCode).toBe(200);
    const searchResponse = await request.post("/cats/search").send({
      fullname: "Thor",
    });
    const body = JSON.parse(searchResponse.text);
    //its grabbing all that match exact, could be more
    expect(body.cats.length).toBeGreaterThan(0);

    await request.delete(`/cats/${body.catsId}`);
  });
});
