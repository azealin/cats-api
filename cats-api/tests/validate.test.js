//validation tests
const validation = require("../functions/helpers/validation");

describe("Validation Helper File Tests", () => {
  test(`The file exists`, () => {
    expect(validation).toBeTruthy();
  });
  it(`should validate cat parameters`, () => {
    expect(
      validation.validateCat({
        fullname: "Mr Bigglesworth",
        color: "white",
        age: 5,
        ownersName: "Kel'Thuzad",
      })
    ).toBe(true);
  });
  it(`should fail invalid cat parameters (name)`, () => {
    expect(
      validation.validateCat({
        fullname: 1,
        color: "white",
        age: 5,
        ownersName: "Kel'Thuzad",
      })
    ).toBe(false);
  });
  it(`should fail invalid cat parameters (age)`, () => {
    expect(
      validation.validateCat({
        fullname: "Mr ",
        color: "white",
        age: "age",
        ownersName: "Kel'Thuzad",
      })
    ).toBe(false);
  });
  it(`should fail invalid cat parameters (empty)`, () => {
    expect(
      validation.validateCat({
        fullname: "",
        color: "",
        age: "",
        ownersName: "",
      })
    ).toBe(false);
  });
  it(`should fail invalid cat parameters (empty object)`, () => {
    expect(validation.validateCat({})).toBe(false);
  });
  it(`should fail invalid cat parameters (age <= 0)`, () => {
    expect(
      validation.validateCat({
        fullname: "Mr Bigglesworth",
        color: "white",
        age: 0,
        ownersName: "Kel'Thuzad",
      })
    ).toBe(false);
    expect(
      validation.validateCat({
        fullname: "Mr Bigglesworth",
        color: "white",
        age: -100,
        ownersName: "Kel'Thuzad",
      })
    ).toBe(false);
  });
});
