let request = require("supertest");

request = request("https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com");
//hard setting the URL, maybe can pull this from somewhere later?
describe("Create Endpoint Tests", () => {
  it("should create a cat", async () => {
    const response = await request.post("/cats").send({
      fullname: "Mr Bigglesworth",
      color: "white",
      age: 5,
      ownersName: "Kel'Thuzad",
    });

    expect(response.statusCode).toBe(200);
    const body = JSON.parse(response.text);
    expect(body.message).toEqual(
      `Sucessfully submitted cat with the name Mr Bigglesworth`
    );
    await request.delete(`/cats/${body.catsId}`);
  });
});
