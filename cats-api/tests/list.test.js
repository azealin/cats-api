let request = require("supertest");

request = request("https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com");
//hard setting the URL, maybe can pull this from somewhere later?
describe("List Endpoint Tests", () => {
  it("should list all cats", async () => {
    const response = await request.get("/cats").send();
    expect(response.statusCode).toBe(200);
    const body = JSON.parse(response.text);
    //its grabbing atleast more than 1, is there a way to check all datas being passsed?
    expect(body.cats.length).toBeGreaterThan(1);
  });
});
