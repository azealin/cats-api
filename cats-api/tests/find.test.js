let request = require("supertest");

request = request("https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com");
//hard setting the URL, maybe can pull this from somewhere later?
jest.setTimeout(10000);
describe("Find Endpoint Tests", () => {
  it("should find a cat", async () => {
    const createResponse = await request.post("/cats").send({
      fullname: "Mr Bigglesworth",
      color: "white",
      age: 5,
      ownersName: "Kel'Thuzad",
    });
    const catsId = JSON.parse(createResponse.text).catsId;

    const response = await request.get(`/cats/${catsId}`);
    expect(response.statusCode).toBe(200);
    const body = JSON.parse(response.text);
    expect(body).toMatchObject({
      fullname: "Mr Bigglesworth",
      color: "white",
      age: 5,
      ownersName: "Kel'Thuzad",
    });

    await request.delete(`/cats/${catsId}`);
  });
});
