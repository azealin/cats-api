"use strict";
const AWS = require("aws-sdk");
const validation = require("./helpers/validation");

AWS.config.setPromisesDependency(require("bluebird"));

const dynamoDb = new AWS.DynamoDB.DocumentClient();
//this currently only matches by exact name, can modify to probably take in contains/start with matching ,
//also returns multiple results if cats share the same name, can filter further by accepting an owner
module.exports.search = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);
  const fullname = requestBody.fullname;
  if (!validation.validateFullName(fullname)) {
    console.error("Validation Failed");
    callback(new Error("Unable to search due to invalid search parameters."));
    return;
  }
  //manually creating condition since we are just searching by name
  let KeyConditionExpression = "fullname = :fullname";
  let ExpressionAttributeValues = {
    ":fullname": fullname,
  };
  const params = {
    TableName: process.env.CATS_TABLE,
    IndexName: process.env.NAME_INDEX,
    KeyConditionExpression: KeyConditionExpression,
    ExpressionAttributeValues: ExpressionAttributeValues,
  };
  console.log(params);
  dynamoDb
    .query(params)
    .promise()
    .then((data) => {
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          cats: data.Items,
        }),
      };
      callback(null, response);
    })
    .catch((error) => {
      console.error(error);
      callback(
        new Error(`Error searching for a cat with the name ${fullname}.`)
      );
      return;
    });
};
