"use strict";
const AWS = require("aws-sdk");

AWS.config.setPromisesDependency(require("bluebird"));

const dynamoDb = new AWS.DynamoDB.DocumentClient();
module.exports.delete = (event, context, callback) => {
  const params = {
    TableName: process.env.CATS_TABLE,
    Key: {
      catsId: event.pathParameters.id,
    },
  };

  dynamoDb
    .delete(params)
    .promise()
    .then((result) => {
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: `Sucessfully deleted the cat`,
        }),
      };
      callback(null, response);
    })
    .catch((error) => {
      console.error(error);
      callback(new Error("Couldn't delete cat."));
      return;
    });
};
