module.exports = {
  validateCat: (catObject) => {
    const fullname = catObject.fullname;
    const color = catObject.color;
    const age = catObject.age;
    const ownersName = catObject.ownersName;
    //basic type validation
    if (
      typeof fullname !== "string" ||
      typeof color !== "string" ||
      typeof age !== "number" ||
      typeof ownersName !== "string"
    ) {
      return false;
    }
    //validate that only these four attributes were passed in
    if (!Object.keys(catObject).length == 4) {
      return false;
    }
    //some boundaries as valid values
    if (age <= 0) {
      return false;
    }
    // require color, ownersName
    if (color === "" || ownersName === "") {
      return false;
    }
    return true;
  },
  validateFullName: (fullname) => {
    if (typeof fullname !== "string") {
      return false;
    }
    return true;
  },
};
