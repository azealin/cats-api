"use strict";
const AWS = require("aws-sdk");

AWS.config.setPromisesDependency(require("bluebird"));

const dynamoDb = new AWS.DynamoDB.DocumentClient();
module.exports.find = (event, context, callback) => {
  const params = {
    TableName: process.env.CATS_TABLE,
    Key: {
      catsId: event.pathParameters.id,
    },
  };

  dynamoDb
    .get(params)
    .promise()
    .then((result) => {
      console.log(result);
      if (result.Item) {
        const response = {
          statusCode: 200,
          body: JSON.stringify(result.Item),
        };

        callback(null, response);
      } else {
        const response = {
          statusCode: 404,
          body: JSON.stringify({
            message: `No cat found with id: ${event.pathParameters.id}`,
          }),
        };

        callback(null, response);
      }
    })
    .catch((error) => {
      console.error(error);
      callback(new Error("Error fetching cat."));
      return;
    });
};
