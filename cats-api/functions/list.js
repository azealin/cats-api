"use strict";
const AWS = require("aws-sdk");

AWS.config.setPromisesDependency(require("bluebird"));

const dynamoDb = new AWS.DynamoDB.DocumentClient();
module.exports.list = (event, context, callback) => {
  var params = {
    TableName: process.env.CATS_TABLE,
    ProjectionExpression: "catsId, fullname, color, age, ownersName",
  };

  const onScan = (err, data) => {
    if (err) {
      console.log(
        "Failed to load data. Error JSON:",
        JSON.stringify(err, null, 2)
      );
      callback(err);
    } else {
      return callback(null, {
        statusCode: 200,
        body: JSON.stringify({
          cats: data.Items,
        }),
      });
    }
  };

  dynamoDb.scan(params, onScan);
};
