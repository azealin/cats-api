"use strict";
const validation = require("./helpers/validation");
const uuid = require("uuid");
const AWS = require("aws-sdk");

AWS.config.setPromisesDependency(require("bluebird"));

const dynamoDb = new AWS.DynamoDB.DocumentClient();
//function to create a cat POST /cats
module.exports.create = (event, context, callback) => {
  console.log(event.body);
  const requestBody = JSON.parse(event.body);
  if (!validation.validateCat(requestBody)) {
    console.error("Validation Failed");
    callback(new Error("Unable to submit due to validation errors."));
    return;
  }
  const fullname = requestBody.fullname;
  const color = requestBody.color;
  const age = requestBody.age;
  const ownersName = requestBody.ownersName;

  submitCat(catInfo(fullname, color, age, ownersName))
    .then((result) => {
      callback(null, {
        statusCode: 200,
        body: JSON.stringify({
          message: `Sucessfully submitted cat with the name ${fullname}`,
          catsId: result.catsId,
        }),
      });
    })
    .catch((err) => {
      console.log(err);
      callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          message: `Unable to create cat with the name ${fullname}}`,
        }),
      });
    });
};
//helper function to put in the table, async
const submitCat = (cat) => {
  const catTable = {
    TableName: process.env.CATS_TABLE,
    Item: cat,
  };
  return dynamoDb
    .put(catTable)
    .promise()
    .then((response) => cat);
};
//basic structure of a cat
const catInfo = (fullname, color, age, ownersName) => {
  const timestamp = new Date().getTime();
  return {
    catsId: uuid.v1(),
    fullname: fullname,
    color: color,
    age: age,
    ownersName: ownersName,
    submittedAt: timestamp,
    updatedAt: timestamp,
  };
};
