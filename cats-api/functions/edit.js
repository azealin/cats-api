"use strict";
const AWS = require("aws-sdk");
const validation = require("./helpers/validation");

AWS.config.setPromisesDependency(require("bluebird"));

const dynamoDb = new AWS.DynamoDB.DocumentClient();
module.exports.edit = (event, context, callback) => {
  const requestBody = JSON.parse(event.body);
  //validate data
  if (!validation.validateCat(requestBody)) {
    console.error("Validation Failed");
    callback(new Error("Unable to submit due to validation errors."));
    return;
  }
  //add timestamp
  requestBody["updatedAt"] = new Date().getTime();
  //dynamically creating expression for updates based on passed in object
  let updateExpression = "set";
  let ExpressionAttributeNames = {};
  let ExpressionAttributeValues = {};
  for (const property in requestBody) {
    updateExpression += ` #${property} = :${property} ,`;
    ExpressionAttributeNames["#" + property] = property;
    ExpressionAttributeValues[":" + property] = requestBody[property];
  }
  //remove the trailing comma
  updateExpression = updateExpression.slice(0, -1);
  const params = {
    TableName: process.env.CATS_TABLE,
    Key: {
      catsId: event.pathParameters.id,
    },
    UpdateExpression: updateExpression,
    ExpressionAttributeNames: ExpressionAttributeNames,
    ExpressionAttributeValues: ExpressionAttributeValues,
  };

  dynamoDb
    .update(params)
    .promise()
    .then((result) => {
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          message: `Successfully updated ${requestBody.fullname}`,
        }),
      };
      callback(null, response);
    })
    .catch((error) => {
      console.error(error);
      callback(new Error("Couldn't update cat."));
      return;
    });
};
