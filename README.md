# cats-api

A project for cat lovers

## Getting started

- cd cats-api
- npm install

## Test and Deploy

serverless deploy -s {stage}

## Authors

Ashley Yang

# Serverless Framework Cats Node HTTP API on AWS

A simple HTTP API with Node.js running on AWS Lambda and API Gateway using the Serverless Framework.

## Usage

See https://app.swaggerhub.com/apis/mewlul/CatsAPI/1.0.0 for API Documentation

## Testing

- cd cats-api
- npm test

### Deployment

```
$ serverless deploy -s {stage}
```

note: currently a public API with no authentication, we would want to add an authentiator of sorts to protect the data most likely/prevent just anyone from calling it

### Invocation

After successful deployment, you can call the created application via HTTP:

CREATE

```
curl -H "Content-Type: application/json" -X POST -d '{"fullname":"Mr Bigglesworth","color": "white", "age":5, "ownersName":"Kel'\''Thuzad"}' https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com/cats

```

GET 1

Pulls a cat by its ID

```
curl -H "Content-Type: application/json" -X GET https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com/cats/{id}
```

GET ALL

```
curl -H "Content-Type: application/json" -X GET https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com/cats
```

UPDATE

```
curl -H "Content-Type: application/json" -X PUT -d '{"fullname":"Mr Bigglesworth","color": "white", "age":5, "ownersName":"Kel'\''Thuzad"}' https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com/cats/{id}

```

DELETE

Deletes a cat by its ID

```
curl -H "Content-Type: application/json" -X DELETE https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com/cats/{id}
```

SEARCH (exact name)

```
curl -H "Content-Type: application/json" -X POST -d '{"fullname":"Mr Bigglesworth"}' https://iucf1h7bz4.execute-api.us-east-1.amazonaws.com/cats/search

```

### Local development

You can invoke your function locally by using the following command:

```bash
serverless invoke local --function {function-name}
```

Which should result in response similar to the following:

```
{
  "statusCode": 200,
  "body": "{}"
}
```
